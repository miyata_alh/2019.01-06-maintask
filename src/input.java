package com.example.my_boss.questrip;

import com.example.my_boss.questrip.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by daisuke on 16/10/22.
 */

//宮田）Activityを継承してinputクラス作成
public class input extends Activity {

	//宮田）メソッド内で使用するフィールドの設定
    private InputMethodManager inputMethodManager;
    private LinearLayout mainLayout;
    private EditText editText;


    @Override
	/**宮田）
	*onCreateはActivityが作成された際にリソースの初期化を行う、Androidのライフサイクルメソッド
	*ライフサイクルメソッドとは、Actiivtyが起動～終了する一連の流れの中で、
	*Android側から必ずコールされるメソッドのこと
	**/
    protected void onCreate(Bundle savedInstanceState) {
    	//宮田）superクラスの継承
        super.onCreate(savedInstanceState);
    	//宮田）Activity上に表示するview(テキストなど部品を配置）を設定
        setContentView(R.layout.input);

    /**宮田）
    *Bundle savedInstanceStatの初期化は基本null
    *特定の場合に値が入ってきます。
    *例えば構成の変更があったりした際（Activityが作り直された時）画面を回転させるなど
    	**/

    }
}