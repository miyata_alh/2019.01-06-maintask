package com.example.my_boss.questrip;

import android.app.Application;
import android.graphics.Bitmap;

/**
 * Created by takayayuuki on 2016/11/05.
 */

//宮田）Applicationを継承してglobal_valuesクラス作成
public class global_values extends Application {
    //グローバルに使用する変数たち
    String user;
    String pass;
    double latitude_final; //宮田）緯度
    double longitude_final; //宮田）経度
    Bitmap bitmap;

    public static Context globalContext;

    public global_values(){ //宮田）ここでは何もしない

    }

    //ぜんぶ初期化するメソッド
    public void GlobalsAllInit() {
        user = "test";
        pass = "testpass";
        latitude_final = 0;
        longitude_final = 0;

    }
}