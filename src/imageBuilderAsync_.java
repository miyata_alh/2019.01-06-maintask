package com.example.my_boss.questrip;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.text.html.ImageView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;

/**
 * Created by takayayuuki on 2016/10/24.
 */

//宮田）AsyncTaskを継承してimageBuilderAsyncクラス作成
public class imageBuilderAsync extends AsyncTask<Uri.Builder, Void, Bitmap> {
	//宮田）変数imageView宣言
    private ImageView imageView;

	//宮田）imageBuilderAsyncメソッド呼び出し
    public imageBuilderAsync(){ //宮田）ここではなにもしない
    	
    }


    public imageBuilderAsync(ImageView imageView){
    	//宮田）キャラ画像取得
        this.imageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(Uri.Builder... params) {
    	//宮田）変数connection　nullで初期化
        HttpURLConnection connection = null;
        //宮田）変数inputStream　nullで初期化
    	InputStream inputStream = null;
    	//宮田）変数bitmap　nullで初期化
        Bitmap bitmap = null;

        try{
        	//宮田）URLの作成　urlインスタンス生成　文字列に変換して配列に格納して代入
            URL url = new URL(params[0].toString());
        	//宮田）接続用HttpURLConnectionオブジェクト作成
            connection = (HttpURLConnection)url.openConnection();
        	//宮田）リクエストメソッドの設定
            connection.setRequestMethod("GET");
        	//宮田）接続
            connection.connect();
        	//宮田）connection(HttpURLConnection)に対してgetInputStreamで取得
            inputStream = connection.getInputStream();

        	//宮田）inputStreamで入力した文字列をdecodeStreamで復元化してbitmapに格納
            bitmap = BitmapFactory.decodeStream(inputStream);
        		//宮田）無効な書式の URL が発生したとき例外
        	
        }catch (MalformedURLException exception){
        		//宮田）ストリーム、ファイル、およびディレクトリを使用して情報にアクセスするときの例外
        }catch (IOException exception){

        }finally { //宮田）必ずする処理
        	//宮田）接続されているならば、切断する
            if (connection != null){
                connection.disconnect();
            }
            try{
            	//宮田）文字列が入力されたら閉じる
                if (inputStream != null){
                    inputStream.close();
                }
            		//宮田）ストリーム、ファイル、およびディレクトリを使用して情報にアクセスするときの例外
            }catch (IOException exception){
            }
        }

        return bitmap;
    }


//    @Override
//    protected void onPostExecute(Bitmap result){
//        // インターネット通信して取得した画像をImageViewにセットする
//        this.imageView.setImageBitmap(result);
//    }

}

