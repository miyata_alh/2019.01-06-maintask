package com.example.my_boss.questrip;

import java.awt.Button;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.text.View;
import javax.swing.text.html.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

/**
 * Created by takayayuuki on 2016/11/05.
 */

//宮田）Activityクラスを継承してquestrip_collection_Activityを作成
public class questrip_collection_Activity extends Activity {
	//宮田）IMAGE_NUMを100を代入して初期化
    private int IMAGE_NUM = 100;

    private List<Bitmap> imgList = new ArrayList<Bitmap>();  //ご当地キャラ画像を格納するList
    private GridView gridview;  //gridviewオブジェクト

    private String url_get_server;  //サーバへgetリクエストするためのURL
    private String charID[] = new String[IMAGE_NUM];  //サーバからgetしたキャラIDの格納配列
    private String charID_fromserver;   //サーバからgetしたJSON

    private String url_get_char_fromID = new String();  //ご当地キャラAPIへ送るURL
    private String api_key_gotouchi = "580e584e09edb";  //ご当地キャラAPI使用に用いるAPIキー
    private String string_char;     //ご当地キャラAPIから返って来たJSON


    //AsyncTaskオブジェクト生成
    private getItemDataAsync get_itemData;
    private imageBuilderAsync img_builder;
    private get_server_Async get_server_async;


    private String img_url = new String();        //JSONデータからimage部分のみをパースしてここに入れる．その後imageBuilderAsyncへ
    private Bitmap image_of_character;                  //ご当地キャラの画像データ
    private Uri.Builder builder;                        //URL→Bitmap変換のURL

    global_values global;   //global変数使うためのオブジェクト

    private Button button_top;  //Topへ戻るボタン


    private String userID ="takaya";





	/**宮田）protectedはメソッド、変数のみ付与できる。同一クラス、同一パッケージ、サブクラスから参照できる。
	  *宮田）
	  *onCreateはActivityが作成された際にリソースの初期化を行う、Androidのライフサイクルメソッド
	  *ライフサイクルメソッドとは、Actiivtyが起動～終了する一連の流れの中で、
	  *Android側から必ずコールされるメソッドのこと
	**/
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questrip_collection);
    	//ここまでがワンセット

    	//宮田）画面上表示されるトップボタンを設定
        button_top = (Button)findViewById(R.id.button_top);
    	//宮田）トップボタン押下後に発生させるイベントを設定
        button_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ボタンがクリックされた時に呼び出されます
            	/**宮田）Intentで画面遷移
            	  *アプリケーション間の連携機能で異なるアプリケーションを繋ぐランタイム(機能・モジュール)
                **/
            	Intent intent=new Intent();
            	//宮田）遷移先のクラスネームをセットして画面遷移
                intent.setClassName("com.example.my_boss.questrip","com.example.my_boss.questrip.questrip_root_Activity");
                //宮田）遷移先の画面を起動
            	startActivity(intent);
            }
        });

        //グローバル変数を取得
        global = (global_values)this.getApplication();



//        userID = global.user;
//
//        charID[0] = "3891";
//        charID[1] = "3893";
//        charID[2] = "2050";


        //ここでサーバからキャラID取得
//        url_get_server = "http://192.168.20.23:3000/zukan/:"+global.user;
        url_get_server = "http://192.168.20.32:3000/zukan/:"+userID;
//        url_get_server = "http://192.168.12.11:3000/zukan/:"+userID;

    	//宮田）get_server_asyncインスタンス生成
        get_server_async = new get_server_Async();
        try {
        	//宮田）url_get_serverを実行して取得したキャラIDを設定
            charID_fromserver = get_server_async.execute(url_get_server).get();
        	//宮田）変数jsonインスタンス生成　変数json内にcharID_fromserverを設定
            JSONObject json = new JSONObject(charID_fromserver);
        	//宮田）countがIMAGE_NUM（100）より大きくなるまでループ（キャラIDが100より大きくなるまで）
            for(int count = 0;count < IMAGE_NUM;count++) {
            	//宮田）取得したIDがnullだったら処理終了
                if(json.getString("id"+count) == null){
                    break;
                }
            	//宮田）ID+1をして配列に格納
                charID[count]=json.getString("id" + count);
            }
        	/**宮田）あるスレッドが待ち状態、休止状態、または占有されているとき、
        	  *アクティビティーの前かその間のいずれかにそのスレッドで割り込みが発生した場合にスロー
        	**/
        } catch (InterruptedException e) {
        	//宮田）スタックトレース（通過したメソッドのリスト）
            e.printStackTrace();
        	//宮田）例外をスローすることによって中断したタスクの結果を取得しようとしたときにスロー
        } catch (ExecutionException e) {
        	//宮田）スタックトレース（通過したメソッドのリスト）
            e.printStackTrace();
        	//宮田）JSONObject内でエラーがあった際にスロー
        } catch (JSONException e) {
        	//宮田）スタックトレース（通過したメソッドのリスト）
            e.printStackTrace();
        }

//        Toast toast2 = Toast.makeText(getApplicationContext(), global.user, Toast.LENGTH_SHORT);
//        toast2.show();



        //キャラIDからご当地キャラAPIへ→画像取得


    	//宮田）配列の数がcharIDより大きくなるまでループ（最大100）
        for(int count = 0;count<charID.length;count++){
        	//宮田）取得したIDがnullだったら処理終了
            if (charID[count] == null){
                break;
            }
        	//宮田）変数jsonData_charをnullで初期化
            JSONObject jsonData_char = null;

        	//宮田）ご当地キャラAPIへ送るURLにapi_key_gotouchi(580e584e09edb)+ID
            url_get_char_fromID = "http://localchara.jp/services/api/info/character?api_key="
                    + api_key_gotouchi + "&id=" + charID[count];


        	//宮田）get_itemDataインスタンス生成
            get_itemData = new getItemDataAsync();

            try {
            	//宮田）url_get_char_fromIDを実行して取得したjson(キャラデータ)を代入
                string_char = get_itemData.execute(url_get_char_fromID).get();
            	//宮田）jsonData_charインスタンスを生成
                jsonData_char = new JSONObject(string_char);
            	//宮田）JSONObjectの型、変数dataにJSONObjectの結果（キャラデータ）を代入
                JSONObject data = jsonData_char.getJSONObject("result");

            	//宮田）JSONデータからimage部分（画像）のみパース（解析）して設定
                img_url = data.getString("image");
            	//宮田）urlかららimage部分（画像）のみパース（解析）して.buildUpon()でこのUriから属性をコピーして、新しいビルダーを構築
                builder = Uri.parse(img_url).buildUpon();

            	//宮田）img_builderインスタンスを生成
                img_builder = new imageBuilderAsync();
            	//宮田）builderを実行して取得した画像データをBitmapのimage_of_characterに代入
                image_of_character = img_builder.execute(builder).get();

                //imglistにbitmap格納
                imgList.add(image_of_character);

            /**宮田）あるスレッドが待ち状態、休止状態、または占有されているとき、
        	  *アクティビティーの前かその間のいずれかにそのスレッドで割り込みが発生した場合にスロー
        	**/
            } catch (InterruptedException e) {
            	//宮田）スタックトレース（通過したメソッドのリスト）
                e.printStackTrace();
            	//宮田）例外をスローすることによって中断したタスクの結果を取得しようとしたときにスロー
            } catch (ExecutionException e) {
            	//宮田）スタックトレース（通過したメソッドのリスト）
                e.printStackTrace();
            	//宮田）JSONObject内でエラーがあった際にスロー
            } catch (JSONException e) {
            	//宮田）スタックトレース（通過したメソッドのリスト）
                e.printStackTrace();
            }


        }


    	//宮田）変数 adapterインスタンスを生成
        BitmapAdapter adapter = new BitmapAdapter(getApplicationContext(), R.layout.list_item, imgList);

    	//宮田）GridViewはスクロール可能なアイテムのリストを表示するViewグループ
        GridView gridView = (GridView) findViewById(R.id.gridview);
    	//宮田）GridviewにアダプターGridAdapterをセット
        gridView.setAdapter(adapter);

    }



	//宮田）ArrayAdapter<Bitmap>を継承したBitmapAdapterクラス
    public class BitmapAdapter extends ArrayAdapter<Bitmap> {

    	//宮田）resourceIdフィールド
        private int resourceId;

    	//宮田）BitmapAdapterメソッド
        public BitmapAdapter(Context context, int resource, List<Bitmap> objects) {
        	//宮田）親ArrayAdapter<Bitmap>から参照
            super(context, resource, objects);
        	//宮田）resourceIdにresourceを代入
            resourceId = resource;
        }

        @Override
    	/**宮田）convertView以前まで表示されていたView)を使用してリサイクル
    	  *Viewが再利用されると引数のconvertViewに見えなくなったView(今の例だと0番目のView)が設定されてgetViewが呼び出されることになる。
    	  *convertViewがnullかどうかを判定してnullじゃなかったら
    	  *わざわざnewすることなく使いまわしてやりましてやることでメモリが節約できて速度も上がる
    	**/
        public View getView(int position, View convertView, ViewGroup parent) {

        	//宮田）convertViewがnullだったら
            if (convertView == null) {
            	//宮田）LayoutInflaterからのviewの生成
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                //宮田）convertView（以前まで表示されていたView）にinflaterのinflateを設定
            	convertView = inflater.inflate(resourceId, null);
            }

        	//宮田）変数view(画面上に表示される画像)に以前表示されていたものを再利用
            ImageView view = (ImageView) convertView;
        	//宮田）画面にBitmapに格納した画像を設定
            view.setImageBitmap(getItem(position));

        	//宮田）画面表示
            return view;
        }
    }
}