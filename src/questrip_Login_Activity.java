package com.example.my_boss.questrip;

import java.awt.Button;

import javax.swing.text.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

//AppCompatActivityを継承してquestrip_Login_Activityを作成
public class questrip_Login_Activity extends AppCompatActivity {
	//宮田）メソッドで使用するフィールドの設定
    private Login_async login_async;
    private String root_url = "http://172.20.11.112:3000";
    private String str_email;
    private String str_pass;

    private EditText edit_email;
    private EditText edit_pass;


    private questrip_Login_Activity main;



    @Override
	/**宮田）protectedはメソッド、変数のみ付与できる。同一クラス、同一パッケージ、サブクラスから参照できる。
	  *宮田）
	  *onCreateはActivityが作成された際にリソースの初期化を行う、Androidのライフサイクルメソッド
	  *ライフサイクルメソッドとは、Actiivtyが起動～終了する一連の流れの中で、
	  *Android側から必ずコールされるメソッドのこと
	**/
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questrip__login_);//宮田）ここまでがワンセット


    	//宮田）画面上入力されるemail,passそして押下ボタン
        edit_email = (EditText) findViewById(R.id.editText1);
        edit_pass = (EditText) findViewById(R.id.editText2);
        Button button = (Button) findViewById(R.id.button);

    	//宮田）root_url （"http://172.20.11.112:3000"） + "/login";ログイン画面遷移
        root_url = root_url + "/login";

//
//        str_email = edit_email.getText().toString();
//        str_pass = edit_pass.getText().toString();
//


//        login_async = new Login_async(this);
//        login_async.execute(to_login_async);


//
    	//宮田）クリック処理
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ボタンがクリックされた時に呼び出されます
            	//宮田）login_asyncインスタンスの生成
                login_async = new Login_async();
            	//宮田）urlの実行、入力されたemail＆パスをテキストで受け取り、文字列に変換を実行
                login_async.execute(root_url,edit_email.getText().toString(),edit_pass.getText().toString());
//                Toast.makeText(getApplicationContext(), edit_email.getText().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void result_job(String result){
    	/**宮田）Toastでメッセージを表示できる
    	 *下記"result"の部分が表示される
    	 *LENGTH_LONGは表示される時間の長さを表している
    	 *今回はクリック処理で入力、実行された内容を表示
    	**/
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
    }
}