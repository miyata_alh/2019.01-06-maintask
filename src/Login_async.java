package com.example.my_boss.questrip;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.os.AsyncTask;

/**
 * Created by takayayuuki on 2016/10/25.
 */

//宮田）AsyncTaskを継承、Login_asyncクラス作成
public class Login_async extends AsyncTask<String ,Void ,String> {
    //宮田）クエストリップのログイン記録 _mainを変数宣言
    private questrip_Login_Activity _main;
    //宮田）ジェイソンオブジェクトインスタンスを生成する
    private JSONObject jsonObject = new JSONObject();

	//宮田）Login_asyncメソッド呼び出す
    public Login_async(){ //宮田）ここではなにもしない

    }

    public Login_async(questrip_Login_Activity main) {
    	//宮田）superメソッドを呼び出す
        super();
        //宮田）変数_mainにmainを代入
        _main = main;
    }


    @Override
    protected String doInBackground(String... params) {
    	//宮田）request_urlを初期化　配列要素を0個作成　パラメータにリクエストURLを渡す
        String  request_url = params[0];
        //宮田）usernameを初期化　配列要素を1個作成　パラメータにユーザーネームを渡す
        String  username = params[1];
        //宮田）passwordを初期化　配列要素を2個作成　パラメータにパスワードを渡す
        String  password = params[2];
		//宮田）変数readerをnullで初期化
        BufferedReader reader = null;
        //宮田）変数osをnullで初期化
        OutputStream os = null;
        //宮田）変数urlConをnullで初期化
        HttpURLConnection urlCon = null;

        try {
			//宮田）URL(request_url)インスタンスを生成する
            URL url = new URL(request_url);

			//宮田）変数urlConに接続されたURLを代入
            urlCon = (HttpURLConnection) url.openConnection();
            //宮田）ReadTimeoutに10000をセット
            urlCon.setReadTimeout(10000);
            //宮田）ConnectTimeoutに20000をセット
            urlCon.setConnectTimeout(20000);
            //宮田）RequestMethodにPOSTをセット
            urlCon.setRequestMethod("POST");
            //宮田）DoOutputにtrueをセット
            urlCon.setDoOutput(true);
            //宮田）RequestPropertyに文字コード：utf-8でアプリケーション：ジェイソンをセット
            urlCon.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            //宮田）UseCachesにfalseをセット
            urlCon.setUseCaches(false);

			//urlConの接続メソッドを呼び出す
            //urlCon.connect();


			//宮田）入力されたユーザーネームをusernameに配置
            jsonObject.put("username", username);
            //宮田）入力されたパスワードをpasswordに配置
            jsonObject.put("password", password);

            // データを送信する
            os = urlCon.getOutputStream();

            //宮田）BufferedWriterインスタンスの生成
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            //変数writerにjsonObjectをBuffereに貯めこむ
            writer.write(String.valueOf(jsonObject));
            //flushでBuffereに貯めこまれたものをファイルに書き込み
            writer.flush();
            //宮田）ファイルを閉じる
            writer.close();
            //宮田）osを閉じる
            os.close();

			//int型ステータスに入力されたコードを返す
            int status = urlCon.getResponseCode();

			//条件分岐
            switch (status) {
            	//宮田）正常なURLだったら
                case HttpURLConnection.HTTP_OK:
                	//宮田）変数isに受信データを代入
                    InputStream is = urlCon.getInputStream();
                    //宮田）readerインスタンスを生成し、受信データを読み込む
                    reader = new BufferedReader(new InputStreamReader(is));

					//宮田）変数httpSourceインスタンスを生成
                    String httpSource = new String();
                    //宮田）変数strを宣言
                    String str;
                    //宮田）1行ずつ読み込み、空行になるまで続ける
                    while (null != (str = reader.readLine())) {
                    	//httpSourceに文字列で読み込んだものを代入
                        httpSource = httpSource + str;
                    }

					//宮田）受信データをclose
                    is.close();
                    //宮田）条件に合えば処理終了
                    break;

                //宮田）不正URLだったら
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                	//宮田）処理終了
                    break;

                //宮田）どちらでもない場合
                default:
                	//宮田）処理終了
                    break;
            }
          //宮田）例外をキャッチ
        } catch (Exception e) {
        	//宮田）printStackTraceを返す（通過したメソッド）
            e.printStackTrace();
        } finally {
        	//宮田）例外の発生に関係なく実行
            try {
            	//宮田）読み込みがnullでなければ
                if (reader != null) {
                    //close
                    reader.close();
                }
                //宮田）osがnullでなければ
                if (os != null) {
                	//close
                    os.close();
                }
                //宮田）urlConがnullでなければ
                if (urlCon != null) {
                	//宮田）切断
                    urlCon.disconnect();
                }
              //宮田）例外をキャッチ
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    //宮田）valueOf（stirng型に変換）を返す
    return String.valueOf(jsonObject);
    }

	
    @Override
    //変数_mainに実行結果を返す
    protected void onPostExecute(String result) {
//        _main.result_job(result);
    }
}

