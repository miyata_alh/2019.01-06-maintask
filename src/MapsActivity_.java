package com.example.my_boss.questrip;

import com.example.my_boss.questrip.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

//宮田)インターフェースOnMapReadyCallback(を実装して)に定義されているFragmentActivityを継承してMapsActivityクラスを作成
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

	//宮田)GoogleMapの型　変数mMapを設定
    private GoogleMap mMap;

    @Override
    /**宮田）protectedはメソッド、変数のみ付与できる。同一クラス、同一パッケージ、サブクラスから参照できる。
	  *onCreateはActivityが作成された際にリソースの初期化を行う、Androidのライフサイクルメソッド
	  *ライフサイクルメソッドとは、Actiivtyが起動～終了する一連の流れの中で、
	  *Android側から必ずコールされるメソッドのこと
	**/
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);//宮田）ここまでがワンセット
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

    	//宮田) 変数mapFragmentの初期化
    	SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //宮田) GoogleMapのインスタンス取得
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override

    public void onMapReady(GoogleMap googleMap) {
    	//宮田) googleMapを代入
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
		//宮田) 変数sydney(シドニー)ピン立てポイント　LatLng(緯度、経度)
        LatLng sydney = new LatLng(-34, 151);
        //宮田)ピンのタイトル"Marker in Sydney"を設定
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //宮田)カメラの変更（＝位置、回転、チルトのヘ変更）時に発生するイベント、シドニーにカメラ位置を変更
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}