package com.example.my_boss.questrip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.AsyncTask;

/**
 * Created by takayayuuki on 2016/11/05.
 */

//宮田) AsyncTask <String,Integer,String>を継承してget_server_Asyncクラスを作成
public class get_server_Async extends AsyncTask <String,Integer,String> {
	//宮田) readStフィールド設定
    private String readSt ;

    @Override
    //宮田) protectedはメソッド、変数のみ付与できる。同一クラス、同一パッケージ、サブクラスから参照できる。
    protected String doInBackground(String... params) { //宮田） ...で可変長を表す、いくつでも引数を渡せる
        //宮田)HttpURLConnectionの型 変数conをnullで初期化
    	HttpURLConnection con = null;
    	//宮田)urlをnullで初期化
        URL url = null;
        //宮田) 変数urlStを初期化　配列0個を作成
        String urlSt = params[0];

        try {
            // URLの作成
            url = new URL(urlSt);

            // 接続用HttpURLConnectionオブジェクト作成
            con = (HttpURLConnection)url.openConnection();

            // リクエストメソッドの設定
            con.setRequestMethod("GET");

            // リダイレクトを自動で許可しない設定
            con.setInstanceFollowRedirects(false);

            // URL接続からデータを読み取る場合はtrue
            con.setDoInput(true);

            // URL接続にデータを書き込む場合はtrue
//            con.setDoOutput(true);

            // 接続
            con.connect(); // ①

        	//宮田）con(HttpURLConnection)に対してgetInputStreamで取得して変数inに代入
            InputStream in = con.getInputStream();
            InputStream in = con.getInputStream();
        	//宮田）getInputStreamで取得して変数inに代入したものを読み込み、readStに代入
            readSt = readInputStream(in);
        	//宮田）無効な書式の URL が発生したとき例外
        } catch (MalformedURLException e) {
        	//宮田）スタックトレース（通過したメソッドのリスト）
            e.printStackTrace();
        	//宮田）ストリーム、ファイル、およびディレクトリを使用して情報にアクセスするときの例外
        } catch (IOException e) {
        	//宮田）スタックトレース（通過したメソッドのリスト）
            e.printStackTrace();
        }
//        return param[0];
        return readSt;
    }



	//宮田）throws宣言 IOException,UnsupportedEncodingException
    public String readInputStream(InputStream in) throws IOException, UnsupportedEncodingException {
        //宮田）StringBufferの型で変数sbをnewでインスタンス生成
    	StringBuffer sb = new StringBuffer();
    	//宮田）変数stを空文字で初期化
        String st = "";

    	/**宮田）BufferedReaderの型で変数brをnewでインスタンス生成
    	  *InputStreamReaderで文字コードをUTF-8に変換して、取得したin（url）を読み込む
        **/
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
    	//宮田）空行になるまで読み込み
        while((st = br.readLine()) != null)
        {
        	//宮田）stに追加
            sb.append(st);
        }
        try
        {
        	//宮田）in(InputStream)取得を閉じる
            in.close();
        }
    	//宮田）例外
        catch(Exception e)
        {
        	//宮田）スタックトレース（通過したメソッドのリスト）
            e.printStackTrace();
        }

    	//宮田）文字列に変換して返す
        return sb.toString();
    }
}